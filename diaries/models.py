from django.db import models
import datetime

# Create your models here.
class Diary(models.Model):
	"""日記のモデル"""
	title = models.CharField(max_length=256)
	detail = models.TextField(default='')
	created = models.DateTimeField(default=datetime.datetime.now)
