from django.shortcuts import render, redirect
from diaries.models import Diary

# Create your views here.
def top(request):
	"""トップ画面"""
	return render(request, 'diaries/top.html')

def index(request):
	"""日記一覧画面"""
	data = {
		'd1': '日記１',
		'd2': '日記２',
		'd3': '日記３',
	}
	items = [
		'日記１',
		'日記２',
		'日記３',
	]
	diaries = Diary.objects.all()
	data_dict = {'diaries': diaries}
	#return render(request, 'diaries/index.html', data)
	#return render(request, 'diaries/index.html', {'items': items})
	return render(request, 'diaries/index.html', data_dict)

def detail(request, diary_id):
	"""日記詳細画面"""
	diary = Diary.objects.get(id=diary_id)
	data_dict = {'diary': diary}
	return render(request, 'diaries/detail.html', data_dict)

def create(request):
	"""日記登録画面"""
	if request.method == 'POST':
		diary = Diary.objects.create(
			title = request.POST['title'],
			detail = request.POST['detail'],
			created = request.POST['created'],
		)
		return redirect('detail', diary.id)
	return render(request, 'diaries/create.html')
